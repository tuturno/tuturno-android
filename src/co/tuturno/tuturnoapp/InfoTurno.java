package co.tuturno.tuturnoapp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Build;

public class InfoTurno extends ActionBarActivity {
	
	private static final String TAG="info turno";
	
	private String horaAtencion;
	private boolean pFlag;
	private TextView txtHoraAtencion;
	private Context mContext;
	
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            
        	Bundle mBundle=intent.getExtras();
    		String mHoraAtencion=mBundle.getString("datetime");
    		Long time=Long.parseLong(mHoraAtencion);
    		Date date = new Date(time);  	
        	SimpleDateFormat sdf = new SimpleDateFormat("h:mm a");
        	String formattedTime = sdf.format(date);
    		setHoraAtencion(formattedTime);
        	
        }
    };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext=getApplicationContext();
		setContentView(R.layout.activity_info_turno);
		Log.d(TAG, "started on create");
		txtHoraAtencion=(TextView)findViewById(R.id.txt_hora_atencion);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.d(TAG, "started on resume");
		
		IntentFilter iFilter=new IntentFilter("com.google.android.c2dm.intent.RECEIVE");
        iFilter.setPriority(1);
        registerReceiver(mHandleMessageReceiver, iFilter);		
		Bundle mBundle=getIntent().getExtras();
		horaAtencion=mBundle.getString("hora_atencion");
		pFlag=mBundle.getBoolean("pendingFlag");
		Log.d(TAG, "hora de atencion es: "+horaAtencion);
		if(pFlag==true) {
			setHoraAtencion(horaAtencion);}
	}
	
	@Override
    protected void onPause(){
        super.onPause();
        unregisterReceiver(mHandleMessageReceiver);
    }
	
	public void onClickAlarmas(View view) {
		Intent iAlarmas=new Intent(mContext, ConfigAlarmas.class);
		startActivity(iAlarmas);
	}
	
	private void setHoraAtencion(String hora) {
		txtHoraAtencion.setText(hora);
	}

	@Override
	protected void onNewIntent(Intent intent) {
	    super.onNewIntent(intent);
	    setIntent(intent);
	}
	
	private void setTiempoRestante() {}

}
