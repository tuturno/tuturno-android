package co.tuturno.tuturnoapp;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import edu.gvsu.masl.asynchttp.HttpConnection;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.os.Build;

public class SplashScreen extends ActionBarActivity {

	private static String TAG="splash screen";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	String SENDER_ID = "669206374648";	
	private static int SPLASH_TIME_OUT=1500;
	
	private static final String PROPERTY_REG_ID = "registration_id";
	private static final String PREFERENCES_FILE_NAME = "tu_turno_app";
	private static final String SUSCRIBER_ID="suscriber_id";
	
	private GoogleCloudMessaging gcm;
	private Context context;
	private String regid;
	
	private Handler mHandler;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);
		context=getApplicationContext();
		
		
		new Handler().postDelayed(new Runnable() {		 
            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                
//            	resetRegistrationId(SplashScreen.this);
            	if (checkPlayServices()) {
        	        // If this check succeeds, proceed with normal processing.
        	        // Otherwise, prompt user to get valid Play Services APK.
//        	    	Log.d("GOOGLE PLAY", "device supports google Play Services APK");
//                    gcm = GoogleCloudMessaging.getInstance(SplashScreen.this);
                    regid = getRegistrationId(context);
        
        			if (regid.isEmpty()) {
//        				TareaRegistroGCM tarea = new TareaRegistroGCM();
//        				tarea.execute();
        				Log.d(TAG, "reg id no existe");        				
        				Intent i = new Intent(SplashScreen.this, RegistroUsuario.class);
                        startActivity(i);              
                        
        			}else {
        				Log.d(TAG, "reg id is not empty. Reg id is: "+regid);
        				Intent i = new Intent(SplashScreen.this, InfoTurno.class); 
        				i.putExtra("pendingFlag", false);
                        startActivity(i); 
        			}
                }else {
                    Log.i(TAG, "No valid Google Play Services APK found.");
                }                  
                
                // close this activity
                finish();
            }
        },SPLASH_TIME_OUT);
	}
	
//	@Override
//	protected void onResume() {
//	    super.onResume();
//	    checkPlayServices();
//	}
	
	private boolean checkPlayServices() {
	    int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
	    if (resultCode != ConnectionResult.SUCCESS) {
	        if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
	            GooglePlayServicesUtil.getErrorDialog(resultCode, this,
	                    PLAY_SERVICES_RESOLUTION_REQUEST).show();
	        } else {
	            Log.i(TAG, "This device is not supported.");
	            finish();
	        }
	        return false;
	    }
	    return true;
	}
	
	private String getRegistrationId(Context context) {
	    final SharedPreferences prefs = getGCMPreferences(context);
	    String registrationId = prefs.getString(PROPERTY_REG_ID, "");
	    if (registrationId.isEmpty()) {
	        Log.i(TAG, "Registration ID not found.");
	        return "";
	    }

	    return registrationId;
	}
	
	private SharedPreferences getGCMPreferences(Context context) {
	    // This sample app persists the registration ID in shared preferences, but
	    // how you store the regID in your app is up to you.
		//return getPreferences(Context.MODE_PRIVATE);		
		return getSharedPreferences(PREFERENCES_FILE_NAME,
	            Context.MODE_PRIVATE);
	}
	
	private void storeRegistrationId(Context context, String regId) {
	    final SharedPreferences prefs = getGCMPreferences(context);
//	    int appVersion = getAppVersion(context);
//	    Log.i(TAG, "Saving regId on app version " + appVersion);
	    SharedPreferences.Editor editor = prefs.edit();
	    editor.putString(PROPERTY_REG_ID, regId);
//	    editor.putInt(PROPERTY_APP_VERSION, appVersion);
	    editor.commit();
	}
	
	private void storeSuscriberId(Context context, String usrId) {
	    final SharedPreferences prefs = getGCMPreferences(context);
//	    int appVersion = getAppVersion(context);
//	    Log.i(TAG, "Saving regId on app version " + appVersion);
	    SharedPreferences.Editor editor = prefs.edit();
	    editor.putString(SUSCRIBER_ID, usrId);
//	    editor.putInt(PROPERTY_APP_VERSION, appVersion);
	    editor.commit();
	}
	
	private void resetRegistrationId(Context context) {
	    final SharedPreferences prefs = getGCMPreferences(context);
//	    int appVersion = getAppVersion(context);
//	    Log.i(TAG, "Saving regId on app version " + appVersion);
	    SharedPreferences.Editor editor = prefs.edit();
	    editor.putString(PROPERTY_REG_ID, "");
//	    editor.putInt(PROPERTY_APP_VERSION, appVersion);
	    editor.commit();
	    Log.d(TAG, "regid deleted");
	}
	
	private String getSuscriberId(Context context) {
	    final SharedPreferences prefs = getGCMPreferences(context);
	    String suscriberId = prefs.getString(SUSCRIBER_ID, "");
	    if (suscriberId.isEmpty()) {
	        return "";
	    }

	    return suscriberId;
	}
	
	private void registerDevice() {
			
			JSONObject json = new JSONObject();
			try {
				json.put("phoneNumber","3017349854");
				json.put("name","Duke");
				json.put("email","juanfernandodk@gmail.com");
				json.put("phoneOS","android");
				json.put("phoneToken",regid);
			}catch(JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			new HttpConnection(mHandler).post("http://tuturno.elasticbeanstalk.com/subscriber/save.json", json.toString());
	}
	
	private class TareaRegistroGCM extends AsyncTask<Void, Integer, String> {

		@Override
		protected String doInBackground(Void... params) {
			String msg = "";

			try {
				if (gcm == null) {
					gcm = GoogleCloudMessaging
							.getInstance(getApplicationContext());
				}

				// Nos registramos en los servidores de GCM
				regid = gcm.register(SENDER_ID);

				Log.d(TAG, "Registrado en GCM: registration_id=" + regid);
				// Nos registramos en nuestro servidor
				// boolean registrado = registroServidor(params[0], regid);
				// Guardamos los datos del registro
				storeRegistrationId(context, regid); 
				registerDevice();
			} catch (IOException ex) {
				Log.d(TAG, "Error registro en GCM:" + ex.getMessage());
			}

			return msg;
		}
	}

}
