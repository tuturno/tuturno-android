package co.tuturno.tuturnoapp;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import edu.gvsu.masl.asynchttp.HttpConnection;

import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;


public class RegistroUsuario extends ActionBarActivity {
	
	private static String TAG="actividad registrar usuario";
	String SENDER_ID = "669206374648";	
	
	private static final String PROPERTY_REG_ID = "registration_id";
	private static final String PREFERENCES_FILE_NAME = "tu_turno_app";
	private static final String SUSCRIBER_ID="suscriber_id";
	
	private GoogleCloudMessaging gcm;
	private Context context;
	private String regid;
	private Handler mHandler;
	private EditText mNumber;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registro_usuario);
		mNumber=(EditText)findViewById(R.id.txtNumber);
		
		context=getApplicationContext();
		gcm = GoogleCloudMessaging.getInstance(this);
		
		mHandler = new Handler() {
			  public void handleMessage(Message message) {
			    switch (message.what) {
			    case HttpConnection.DID_START:
			      break;
			    case HttpConnection.DID_SUCCEED:
			      String response = (String) message.obj;
			      Log.d("TAG", "registered. Response is: "+response);
			      Toast.makeText(getApplicationContext(),R.string.toastRegistrado, 
						   Toast.LENGTH_SHORT).show();
			      
			      Toast toastPendientes=Toast.makeText(getApplicationContext(),R.string.toastPendientes, 
						   Toast.LENGTH_LONG);
			      toastPendientes.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 20);
			      toastPendientes.show();
			      
			      try {
			    	  JSONObject jo;					
			    	  jo = new JSONObject(response);					
				      String id=jo.getString("id");
				      Log.d("TAG", "id is: "+id);
				      storeSuscriberId(context, id);
				      Intent intent=new Intent(getApplicationContext(), InfoTurno.class);
				      startActivity(intent);
			      } catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
			      }
			      break;
			    case HttpConnection.DID_ERROR:
			      Exception e = (Exception) message.obj;
			      e.printStackTrace();
			        Log.d("TAG","error when calling service");
			    break;
			    }
			  }
			};		
	}
		
	public void registrarUsuario(View view){
		
		String mTelefono=mNumber.getText().toString();
		TareaRegistroGCM tarea = new TareaRegistroGCM();
		tarea.execute(mTelefono);
	} 
	
	private class TareaRegistroGCM extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			String msg = "";
			
			String mTelefono=params[0];
			Log.d(TAG, "telefono ingresado es: "+mTelefono);
			
			try {
				if (gcm == null) {
					gcm = GoogleCloudMessaging
							.getInstance(getApplicationContext());
				}

				// Nos registramos en los servidores de GCM
				regid = gcm.register(SENDER_ID);

				Log.d(TAG, "Registrado en GCM: registration_id=" + regid);
				// Nos registramos en nuestro servidor
				// boolean registrado = registroServidor(params[0], regid);
				// Guardamos los datos del registro
				storeRegistrationId(context, regid); 
				registerDevice(mTelefono);
			} catch (IOException ex) {
				Log.d(TAG, "Error registro en GCM:" + ex.getMessage());
			}

			return msg;
		}
	}
	
	private void storeRegistrationId(Context context, String regId) {
	    final SharedPreferences prefs = getGCMPreferences(context);
//	    int appVersion = getAppVersion(context);
//	    Log.i(TAG, "Saving regId on app version " + appVersion);
	    SharedPreferences.Editor editor = prefs.edit();
	    editor.putString(PROPERTY_REG_ID, regId);
//	    editor.putInt(PROPERTY_APP_VERSION, appVersion);
	    editor.commit();
	}
	
	private SharedPreferences getGCMPreferences(Context context) {
	    // This sample app persists the registration ID in shared preferences, but
	    // how you store the regID in your app is up to you.
		//return getPreferences(Context.MODE_PRIVATE);		
		return getSharedPreferences(PREFERENCES_FILE_NAME,
	            Context.MODE_PRIVATE);
	}
	
	private void storeSuscriberId(Context context, String usrId) {
	    final SharedPreferences prefs = getGCMPreferences(context);
//	    int appVersion = getAppVersion(context);
//	    Log.i(TAG, "Saving regId on app version " + appVersion);
	    SharedPreferences.Editor editor = prefs.edit();
	    editor.putString(SUSCRIBER_ID, usrId);
//	    editor.putInt(PROPERTY_APP_VERSION, appVersion);
	    editor.commit();
	}
	
	private void registerDevice(String telefono) {
			
			JSONObject json = new JSONObject();
			try {
				json.put("phoneNumber",telefono);
				json.put("name","Duke");
				json.put("email","juanfernandodk@gmail.com");
				json.put("phoneOS","android");
				json.put("phoneToken",regid);
			}catch(JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			new HttpConnection(mHandler).post("http://tuturno.elasticbeanstalk.com/subscriber/save.json", json.toString());
	}

}
