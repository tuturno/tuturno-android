package co.tuturno.tuturnoapp;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GcmIntentService extends IntentService
{
	private static final String TAG="IntentService";
	
    private static final int NOTIF_ALERTA_ID = 1;
 
    public GcmIntentService() {
            super("GcmIntentService");
        }
 
    @Override
        protected void onHandleIntent(Intent intent)
    {
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
 
            String messageType = gcm.getMessageType(intent);
            Bundle extras = intent.getExtras();
 
            if (!extras.isEmpty())
            {
                    if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType))
                    {	
//                    	Log.d(TAG, "hora es: "+extras.getString("datetime"));
//                    	Log.d(TAG, "tipo es: "+extras.getString("type"));
                        mostrarNotification(extras);
                    }
            }
 
            GcmBroadcastReceiver.completeWakefulIntent(intent);
        }
 
    private void mostrarNotification(Bundle payload){	
    	
    	String horaAtencion=payload.getString("datetime");
    	Log.d("info","datetime is: "+horaAtencion);
    	Long time=Long.parseLong(horaAtencion);
    	Log.d("info","datetime longed is: "+time);
    	
    	Date date = new Date(time);  	
    	SimpleDateFormat sdf = new SimpleDateFormat("h:mm a"); // the format of your date
//    	sdf.setTimeZone(TimeZone.getTimeZone("GMT-5"));
    	String formattedDate = sdf.format(date);
    	System.out.println(formattedDate);
    	
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        
        NotificationCompat.Builder mBuilder =
            new NotificationCompat.Builder(this)
                .setSmallIcon(android.R.drawable.stat_notify_sync)
                .setContentTitle("SAPCO").setContentText("Tu hora de atenci�n es: "+formattedDate);
        
        mBuilder.setAutoCancel(true);
 
        Intent notIntent =  new Intent(this, InfoTurno.class);
        notIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notIntent.putExtra("hora_atencion", formattedDate);
        notIntent.putExtra("pendingFlag", true);
        
        
        PendingIntent contIntent = PendingIntent.getActivity(this, 0, notIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        
        mBuilder.setContentIntent(contIntent);
        mNotificationManager.notify(NOTIF_ALERTA_ID, mBuilder.build());
        }
    
//  TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//  notIntent.putExtra("turnoActual", payload.getString("currentTurn"));
//  
//  stackBuilder.addParentStack(TurnoInfo.class);
//   // Adds the Intent to the top of the stack
//  stackBuilder.addNextIntent(notIntent);
//   // Gets a PendingIntent containing the entire back stack
}